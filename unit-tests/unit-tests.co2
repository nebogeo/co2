;; -*- mode: lisp; -*-
;; What Remains 
;; Copyright (C) 2016 What Remains Entertainment Consortium
;;
;; Licence TBD

(do
 ;; NES header follows
 (asm
  ".byte \"NES\",$1a" ;; number of prg-rom blocks
  ".byte $01" ;; number of chr-rom blocks
  ".byte $01" ;; rom control bytes: horizontal mirroring, no sram or trainer, mapper #0
  ".byte $00,$00" ;; filler
  ".byte $00,$00,$00,$00,$00,$00,$00,$00")

 ;; our memory layout
 (defconst sprite-shadow  "$200") ;; where the sprite control data is
 (defconst poke-test "$300")

 ;; code start
 (org #xc000)

 (defvar n 0)
 (defvar k 0)
 (defvar n16 0)
 (defvar n16-h 0)

 (defvar assertpos 0)

 (defun (assert v)
   (if (eq? v 1)
       (ppu-memset ppu-name-table-0 0 assertpos 1 1)
       (ppu-memset ppu-name-table-0 0 assertpos 1 2))
   (inc assertpos))
 
(defun (test-simple) 1)
(defun (test2-argret a) a)
(defun (test3-multifn a)
  (test3-b #xcd)
  a)
(defun (test3-b b) 99)
(defun (test4-secondarg a b) b)
(defun (test5-multifn2 a b)
  (test5-b 10 20 a)
  b)
(defun (test5-b a b c) 0)
(defun (test6-assignarg a)
  (set! a 1) 
  a)

(defun (test7-multiarg a b c d e f g)
  (+ a (+ b (+ c (+ d (+ e (+ f g)))))))

(defun (test8-recur count num)
  (if (eq? count 0) num
    (test8-recur (+ count 1) (- num 1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; interrupts

 (defun (load-palette)
   ;; copy 32 bytes of bg and sprite palette
   (ppu-memcpy ppu-palette 0 0 #x20 palette 0))

 (defint (reset)  
   ;; disable interrupts while we set stuff up
   (init-system)

   ;; disable all graphics.
   (set! reg-ppu-ctl 0)
   (set! reg-ppu-mask 0)
   (set! reg-ppu-scroll 0)
   (set! reg-ppu-scroll 0)
   ;; send stuff to the ppu here...
   (load-palette)

   ;; logic and simple maths
   (assert 1)
   (assert (not 0))
   (assert (eq? 120 120))
   (assert (eq? (+ 3 8) 11))
   (assert (- 2 1))
   (assert (eq? (* 2 1) 2))
   (assert (eq? (* 1 2) 2))
   (assert (eq? (* 5 20) 100))
   (assert (eq? (* 20 5) 100))
   (assert (eq? -4 (+ -2 -2)))
   (assert (eq? 0 (+ -2 2)))

   ;; variables
   (set! n 1)
   (assert n)

   ;; branches
   (assert (if 0 0 1))
   (assert (when 1 1))

   ;; more numbers
   (assert (< 1 3))
   (assert (not (< 254 254)))
   (assert (<= 2 2))
   (assert (> 50 10))
   (assert (and #b00000001 #b01010101))
   (assert (or #b00000000 #b00000001))
   (assert (xor #b11110000 #b11110001))

   ;; inc/dec
   (inc n)
   (assert (eq? n 2))
   (dec n)
   (assert n)

   ;; shifting
   (assert (>> #b00000010 1))
   (assert (>> #b00100000 5))
   (assert (eq? (<< #b00000001 5) 32))

   ;; memory access
   (poke! poke-test 200) 
   (assert (eq? (peek poke-test) 200))
   (poke! poke-test 40 200) 
   (assert (eq? (peek poke-test 40) 200))

   ;; functions
   (assert (test-simple))
   (assert (test2-argret 1))
   (assert (test3-multifn 1))
   (assert (test4-secondarg 0 1))
   (assert (test5-multifn2 0 1))
   (assert (test6-assignarg 0))
   (assert (eq? (test7-multiarg 1 2 3 4 5 6 7) 28))
   (assert (eq? (test8-recur 0 10) 10))

   ;; loops
   (set! n 0)
   (while (< n 4)
     (inc n))
   (assert (eq? n 4))

   (set! k 0)
   (loop n 0 5 (inc k))
   (assert (eq? k 6))

   (set! k 254)
   (assert (not (< k 254)))
   (while (and (< k 254) (not (eq? k 0))) 
     (dec k))
   (assert (eq? k 254))
   (set! k 253)
   (while (and (< k 254) (not (eq? k 0))) 
     (dec k))
   (assert (eq? k 0))

   ;; signed/unsigned compare
   (assert (not (< -10 10)))
   (assert (<s -10 10))
   (assert (not (> 10 -10)))
   (assert (>s 10 -10))
   (assert (not (<= -100 10)))
   (assert (<=s -100 10))
   (assert (<=s -10 -10))

   ;; 16 bit maths

   (+16! n16 0 255)
   (assert (eq? n16 255))
   (assert (eq? n16-h 0))
   (+16! n16 0 1)
   (assert (eq? n16 0))
   (assert (eq? n16-h 1))
   (+16! n16 2 0)
   (assert (eq? n16 0))
   (assert (eq? n16-h 3))
   (-16! n16 0 8)
   (assert (eq? n16 248))
   (assert (eq? n16-h 2))
   (-16! n16 2 10)
   (assert (eq? n16 238))
   (assert (eq? n16-h 0))

   (set! n16 0)
   (set! n16-h 1)
   (-16! n16 0 1)
   (assert (eq? n16 255))
   (assert (eq? n16-h 0))

   ;; stupid cmp error check
   (assert (not (< 255 255))) 
   (set! n 255)
   (assert (not (< n 255)))

   (assert (not (> 1 1)))


   ;; set basic ppu registers.  load background from $0000,
   ;; sprites from $1000, and the name table from $2000.
   (set! reg-ppu-ctl #b10001000)
   (set! reg-ppu-mask #b00011110)

   (asm
    ;; restart interrupts again
    "cli"
    ;; go into in infinite loop - vblank interrupt takes over
    "loop: jmp loop"))


 ;; main loop is here
 (defint (vblank))
 (defint (irq))
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; data follows
;; interrupt vectors (addresses for functions)
 (asm ".word vblank, reset, irq")
  ;; stuff still to tidy up follows...

 (asm "palette:")
 (byte "$0d,$00,$00,$00,$00,$02,$03,$04,$01,$02,$03,$0c,$0c,$1c,$2c,$3c")
 (byte "$0d,$18,$28,$38,$0c,$1c,$2c,$3c,$03,$13,$23,$33,$09,$19,$29,$39")

 (asm "bg:")
  ;; attribute table
 (byte "$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00")
 (byte "$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00")
 (byte "$00,$00,$00,$00,$00,$00,$00,$00,$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0")
 (byte "$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$0f,$0f,$0f,$0f,$0f,$0f,$0f,$0f")
 (asm ".pad $fffa")

;; interrupt vectors (addresses for functions)
 (asm ".word vblank, reset, irq")

 ;; finally all the chr data
 (asm 
  ".incbin \"unit-tests/binary/test.chr\"")
 

 )
